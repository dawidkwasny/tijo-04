package pl.edu.pwsztar.service;

import pl.edu.pwsztar.domain.dto.MovieDto;
import java.util.List;

import pl.edu.pwsztar.domain.dto.CreateMovieDto;

public interface MovieService {

    List<MovieDto> findAll();
    void addMovie(CreateMovieDto arg);
}
